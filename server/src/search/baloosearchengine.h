/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Vishesh Handa <me@vhanda.in>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef BALOOSEARCHENGINE_H
#define BALOOSEARCHENGINE_H

#include "abstractsearchengine.h"

#include <QtCore/QHash>
#include <QtCore/QMutex>
#include <QtCore/QObject>

namespace Akonadi {

class NotificationCollector;

class BalooSearchEngine : public QObject, public AbstractSearchEngine
{
  Q_OBJECT
  public:
    BalooSearchEngine(QObject* parent = 0);
    ~BalooSearchEngine();

    virtual void addSearch(const Collection& collection);
    virtual void removeSearch(qint64 id);

  private Q_SLOTS:

  private:
    NotificationCollector* mCollector;
};

}

#endif // BALOOSEARCHENGINE_H
