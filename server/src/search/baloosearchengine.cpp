/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Vishesh Handa <me@vhanda.in>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "baloosearchengine.h"

#include <akdebug.h>

#include "entities.h"
#include "storage/notificationcollector.h"
#include "storage/selectquerybuilder.h"
#include "notificationmanager.h"

using namespace Akonadi;

BalooSearchEngine::BalooSearchEngine(QObject* parent)
  : QObject(parent)
  , mCollector(new NotificationCollector(this))
{
}

BalooSearchEngine::~BalooSearchEngine()
{

}

void BalooSearchEngine::addSearch(const Collection& collection)
{
  if (collection.queryLanguage() != QLatin1String( "BALOO" )) {
    return;
  }

  const qint64 id = collection.id();
  const QString q = collection.queryString();

  // Do shit with q
}

void BalooSearchEngine::removeSearch(qint64 id)
{
}

