/*
    Copyright (c) 2011 Volker Krause <vkrause@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef AKTEST_H
#define AKTEST_H

#include "akapplication.h"

#define AKTEST_MAIN( TestObject ) \
int main( int argc, char **argv ) \
{ \
  qputenv( "XDG_DATA_HOME", ".local-unit-test/share" ); \
  qputenv( "XDG_CONFIG_HOME", ".config-unit-test" ); \
  AkCoreApplication app( argc, argv ); \
  app.parseCommandLine(); \
  TestObject tc; \
  return QTest::qExec( &tc, argc, argv ); \
}

inline void akTestSetInstanceIdentifier( const QString &instanceId )
{
  AkApplication::setInstanceIdentifier( instanceId );
}

#endif
